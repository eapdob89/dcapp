package com.example.dcapp.dcapp;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.lang.String;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GameActivity extends Activity {
    enum Games {
        WEPLAY_WECOUNT, WEPLAY_THEYCOUNT, THEYPLAY_THEYCOUNT, THEYPLAY_WECOUNT, UNDEFINED;
    }
    /*enum Outcomes {
        WEPLAY_WIN, WEPLAY_WIN_CLEAR, WEPLAY_BAIT, WEPLAY_BAIT_CLEAR, THEYPLAY_WIN,
        THEYPLAY_WIN_CLEAR, THEYPLAY_BAIT, THEYPLAY_BAIT_CLEAR, UNDEFINED;
    }*/

    int x = 0, y = 0, gameType = 162, weCount = 0, theyCount = 0, fine = 100, restWeCount = 0,
            restTheyCount = 0, baitWeCount = 0, baitTheyCount = 0, restAddition = 0,
            weCurrentCount = 0, theyCurrentCount = 0, counterGame = 1;
    Button gameTypeBtn, weBtn, theyBtn, countBtn, removeBtn, newBtn, clearXBtn;
    TextView gameDistrTv, weCountTv, theyCountTv, countTv;
    //private String concl = "";
    String[] games = {"162", "172", "182", "192", "202", "212", "222", "232", "242", "252", "262"};
    Boolean xFlag = false;
    Boolean restWeFlag = false;
    Boolean restTheyFlag = false;
    AlertDialog.Builder myAlert;
    private static final String TAG = "DC GameActivity";
    Games gamesConclusion = Games.UNDEFINED;
    //Outcomes outComes = Outcomes.UNDEFINED;
    String weName, theyName, winName;
    SharedPreferences sPref;
    private static final String NAMES = "names";
    final String SAVED_NAME_1 = "saved_name_we";
    final String SAVED_NAME_2 = "saved_name_they";


    ArrayList<Map<String, String>> listGames;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);

        countTv = (TextView) findViewById(R.id.countTv);
        countBtn = (Button) findViewById(R.id.countBtn);
        weBtn = (Button) findViewById(R.id.weBtn);
        theyBtn = (Button) findViewById(R.id.theyBtn);
        gameTypeBtn = (Button) findViewById(R.id.gameTypeBtn);
        weCountTv = (TextView) findViewById(R.id.weCountTv);
        theyCountTv = (TextView) findViewById(R.id.theyCountTv);
        newBtn = (Button) findViewById(R.id.newGameBtn);
        gameDistrTv = (TextView) findViewById(R.id.gameDistrTv);
        removeBtn = (Button) findViewById(R.id.removeBtn);
        clearXBtn = (Button) findViewById(R.id.clearXBtn);

        countBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                countUp();
            }
        });

        weBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Dialog We or Are
                //set concl

                Log.d("DC GameActivity ", "We Play");

                myAlert = new AlertDialog.Builder(GameActivity.this);
                myAlert.setTitle("Who Count?");
                //myAlert.setMessage("Who Play?");
                myAlert.setPositiveButton("They", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        //implement addition to string concl
                        //concl = concl + "TheyCount";

                        gamesConclusion = Games.WEPLAY_THEYCOUNT;
                        Log.d("DC GameActivity We Play And ", "They Count");

                        /*AlertDialog.Builder builderInner = new AlertDialog.Builder(
                                GameActivity.this);
                        builderInner.setMessage("Who Count?");
                        builderInner.setPositiveButton("They", new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {

                                concl = concl + "TheyCount";
                                Log.d("DC GameActivity ", "They Play And They Count");
                                //dialogInterface.dismiss();
                            }
                        });
                        builderInner.setNegativeButton("We", new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {

                                Log.d("DC GameActivity ", "They Play And We Count");
                                //dialogInterface.dismiss();
                            }
                        });
                        builderInner.setCancelable(true);
                        builderInner.setOnCancelListener(new DialogInterface.OnCancelListener() {
                            public void onCancel(DialogInterface dialog) {
                                concl = "";
                                Toast.makeText(GameActivity.this, "Don't choose",
                                        Toast.LENGTH_LONG).show();
                            }
                        });
                        builderInner.show();*/
                    }
                });
                myAlert.setNegativeButton("We", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        //implement addition to string concl
                        //concl = concl + "WeCount";
                        gamesConclusion = Games.WEPLAY_WECOUNT;
                        Log.d("DC GameActivity We Play And ", "We Count");
                    }
                });
                myAlert.setCancelable(true);
                myAlert.setOnCancelListener(new DialogInterface.OnCancelListener() {
                    public void onCancel(DialogInterface dialog) {
                        //concl = "";
                        gamesConclusion = Games.UNDEFINED;
                        Toast.makeText(GameActivity.this, "Don't choose",
                                Toast.LENGTH_LONG).show();
                    }
                });
                myAlert.show();
            }
        });

        theyBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Dialog We or Are
                //set concl
                //concl = "TheyPlayAnd";
                Log.d("DC GameActivity ", "They Play");

                myAlert = new AlertDialog.Builder(GameActivity.this);
                myAlert.setTitle("Who Count?");
                //myAlert.setMessage("Who Play?");
                myAlert.setPositiveButton("They", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        //implement addition to string concl
                        //concl = concl + "TheyCount";
                        gamesConclusion = Games.THEYPLAY_THEYCOUNT;
                        Log.d("DC GameActivity They Play And ", "They Count");
                    }
                });
                myAlert.setNegativeButton("We", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        //implement addition to string concl
                        //concl = concl + "WeCount";
                        gamesConclusion = Games.THEYPLAY_WECOUNT;
                        Log.d("DC GameActivity They Play And ", "We Count");
                    }
                });
                myAlert.setCancelable(true);
                myAlert.setOnCancelListener(new DialogInterface.OnCancelListener() {
                    public void onCancel(DialogInterface dialog) {
                        //concl = "";
                        gamesConclusion = Games.UNDEFINED;
                        Toast.makeText(GameActivity.this, "Don't choose",
                                Toast.LENGTH_LONG).show();
                    }
                });
                myAlert.show();
            }
        });

        gameTypeBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                myAlert = new AlertDialog.Builder(GameActivity.this);
                myAlert.setIcon(R.drawable.ic_launcher);
                myAlert.setTitle("Choose Game Type");
                final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(GameActivity.this,
                        android.R.layout.select_dialog_singlechoice, games);
                myAlert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });
                myAlert.setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        String strName = arrayAdapter.getItem(i);
                        gameType = Integer.parseInt(strName);

                        AlertDialog.Builder builderInner = new AlertDialog.Builder(
                                GameActivity.this);
                        builderInner.setMessage(strName);
                        builderInner.setTitle("Your Selected Game is");
                        builderInner.setPositiveButton("Ok",
                                new DialogInterface.OnClickListener() {

                                    @Override
                                    public void onClick(
                                            DialogInterface dialogInterface,
                                            int i) {

                                        //implement gametype
                                        dialogInterface.dismiss();
                                    }
                                });
                        builderInner.show();
                    }
                });

                myAlert.show();
            }
        });

        newBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                resetAll();
            }
        });

        removeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //removeLastGame();
            }
        });

        clearXBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clearDigit();
            }
        });

        loadNames();

        listGames = new ArrayList<Map<String,String>>();
    }

    public void onClick(View v) {
        Button btn = (Button)v;
        int temp = Integer.parseInt(v.getTag().toString());

        if(xFlag) {
            x = 0;
            y = 0;
            xFlag = false;
        }

        x = (10 * x) + temp;
        if(x == 0) {
            return;
        }

        if(x > 1001) {
            Log.d(TAG, " onClick more than 1001");
            x = 0;
            countTv.setText("0");
            return;
        }

        String s = Integer.toString(x);
        countTv.setText(s);
        Log.d(TAG, " onClick");
    }

    void countUp() {
        switch (gamesConclusion) {
            case WEPLAY_WECOUNT:
                if(x > gameType) {
                    if(x == 1001) {
                        Log.d(TAG, "End of game");
                        return;
                    } else {
                        Log.d(TAG, "Wrong input");
                        return;
                    }
                }
                if(x == gameType) {
                    //clear game
                    weCount += gameType;
                    theyCount -= fine;

                    if(restWeFlag) {
                        weCount += restWeCount;
                        restAddition = restWeCount;
                        restWeFlag = false;
                    }

                    if(restTheyFlag) {
                        weCount += restTheyCount;
                        restAddition = restTheyCount;
                        restTheyFlag = false;
                    }

                    weCurrentCount = x + restAddition;
                    theyCurrentCount = 0;
                    restAddition = 0;
                    String result = "clearGame";
                    writeGameToList(result);
                }
                else if(x > gameType/2) {
                    //good game for we
                    y = gameType - x;
                    weCount += x;
                    theyCount += y;

                    if(restWeFlag) {
                        weCount += restWeCount;
                        restWeFlag = false;
                    }
                    if(restTheyFlag) {
                        weCount += restTheyCount;
                        restTheyFlag = false;
                    }

                    weCurrentCount = x + restAddition;
                    theyCurrentCount = y;
                    restAddition = 0;
                    String result = "normalGame";
                    writeGameToList(result);
                }
                else if(x == gameType/2) {
                    //hang for we
                    theyCount += gameType/2;
                    if(restWeFlag) {
                        restWeCount += gameType/2;
                    } else {
                        restWeCount = gameType/2;
                        restWeFlag = true;
                    }

                    weCurrentCount = 0;
                    theyCurrentCount = gameType/2;
                    String result = "hanging";
                    writeGameToList(result);
                }
                else if(x == 0) {
                    //full fail clear game for they
                    theyCount += gameType;
                    weCount -= fine;
                    //bait we
                    baitWeCount++;
                    if(baitWeCount > 2) {
                        baitWeCount = 0;
                        weCount -= fine;
                    }
                    if(restWeFlag) {
                        theyCount += restWeCount;
                        restAddition = restWeCount;
                        restWeFlag = false;
                    }
                    if(restTheyFlag) {
                        theyCount += restTheyCount;
                        restAddition = restTheyCount;
                        restTheyFlag = false;
                    }

                    weCurrentCount = 0;
                    theyCurrentCount = gameType + restAddition;
                    String result = "failClearGame";
                    writeGameToList(result);
                }
                else {
                    //bait we
                    theyCount += gameType;
                    baitWeCount++;
                    if(baitWeCount > 2) {
                        baitWeCount = 0;
                        weCount -= fine;
                    }
                    if(restWeFlag) {
                        theyCount += restWeCount;
                        restAddition = restWeCount;
                        restWeFlag = false;
                    }
                    if(restTheyFlag) {
                        theyCount += restTheyCount;
                        restAddition = restTheyCount;
                        restTheyFlag = false;
                    }

                    weCurrentCount = 0;
                    theyCurrentCount = gameType + restAddition;
                    String result = "bait";
                    writeGameToList(result);
                }
                counterGame++;
                xFlag = true;
                break;
            case WEPLAY_THEYCOUNT:
                if(x > gameType) {
                    if(x == 1001) {
                        Log.d(TAG, "End of game");
                    } else {
                        Log.d(TAG, "Wrong input");
                    }
                }
                else if(x == gameType) {
                    //full fail clear game for they
                    weCount -= fine;
                    theyCount += gameType;
                    //bait we
                    baitWeCount++;
                    if(baitWeCount > 2) {
                        baitWeCount = 0;
                        weCount -= fine;
                    }
                    if(restWeFlag) {
                        theyCount += restWeCount;
                        restAddition = restWeCount;
                        restWeFlag = false;
                    }
                    if(restTheyFlag) {
                        theyCount += restTheyCount;
                        restAddition = restTheyCount;
                        restTheyFlag = false;
                    }

                    weCurrentCount = 0;
                    theyCurrentCount = gameType + restAddition;
                    String result = "failClearGame";
                    writeGameToList(result);
                }
                else if(x > gameType/2) {
                    //bait we
                    theyCount += gameType;
                    baitWeCount++;
                    if(baitWeCount > 2) {
                        baitWeCount = 0;
                        weCount -= fine;
                    }
                    if(restWeFlag) {
                        theyCount += restWeCount;
                        restAddition = restWeCount;
                        restWeFlag = false;
                    }
                    if(restTheyFlag) {
                        theyCount += restTheyCount;
                        restAddition = restTheyCount;
                        restTheyFlag = false;
                    }

                    weCurrentCount = 0;
                    theyCurrentCount = gameType + restAddition;
                    String result = "bait";
                    writeGameToList(result);
                }
                else if(x == gameType/2) {
                    //hanging for we
                    theyCount += gameType/2;
                    if(restWeFlag) {
                        restWeCount += gameType/2;
                    } else {
                        restWeCount = gameType/2;
                        restWeFlag = true;
                    }

                    weCurrentCount = 0;
                    theyCurrentCount = gameType/2;
                    String result = "hanging";
                    writeGameToList(result);
                }
                else if(x == 0) {
                    //clear game for we
                    weCount += gameType;
                    theyCount -= fine;
                    if(restWeFlag) {
                        weCount += restWeCount;
                        restAddition = restWeCount;
                        restWeFlag = false;
                    }
                    if(restTheyFlag) {
                        weCount += restTheyCount;
                        restAddition = restTheyCount;
                        restTheyFlag = false;
                    }

                    weCurrentCount = gameType + restAddition;
                    theyCurrentCount = 0;
                    String result = "clearGame";
                    writeGameToList(result);
                }
                else {
                    //good game for we
                    y = gameType - x;
                    weCount += y;
                    theyCount += x;
                    if(restWeFlag) {
                        weCount += restWeCount;
                        restAddition = restWeCount;
                        restWeFlag = false;
                    }
                    if(restTheyFlag) {
                        weCount += restTheyCount;
                        restAddition = restTheyCount;
                        restTheyFlag = false;
                    }

                    weCurrentCount = y + restAddition;
                    theyCurrentCount = x;
                    String result = "normalGame";
                    writeGameToList(result);
                }
                counterGame++;
                xFlag = true;
                break;
            case THEYPLAY_WECOUNT:
                if(x > gameType) {
                    if(x == 1001) {
                        Log.d(TAG, "End of game");
                    } else {
                        Log.d(TAG, "Wrong input");
                    }
                }
                else if(x == gameType) {
                    //full fail for they bait and clear game for we
                    weCount += gameType;
                    theyCount -= fine;
                    baitTheyCount++;
                    if(baitTheyCount > 2) {
                        baitTheyCount = 0;
                        theyCount -= fine;
                    }
                    if(restWeFlag) {
                        weCount += restWeCount;
                        restAddition = restWeCount;
                        restWeFlag = false;
                    }
                    if(restTheyFlag) {
                        weCount += restTheyCount;
                        restAddition = restTheyCount;
                        restTheyFlag = false;
                    }

                    weCurrentCount = gameType + restAddition;
                    theyCount = 0;
                    String result = "failClearGame";
                    writeGameToList(result);
                }
                else if(x > gameType/2) {
                    //bait for they
                    weCount += gameType;
                    baitTheyCount++;
                    if(baitTheyCount > 2) {
                        baitTheyCount = 0;
                        theyCount -= fine;
                    }
                    if(restWeFlag) {
                        weCount += restWeCount;
                        restAddition = restWeCount;
                        restWeFlag = false;
                    }
                    if(restTheyFlag) {
                        weCount += restTheyCount;
                        restAddition = restTheyCount;
                        restTheyFlag = false;
                    }

                    weCurrentCount = gameType + restAddition;
                    theyCurrentCount = 0;
                    String result = "bait";
                    writeGameToList(result);
                }
                else if(x == gameType/2) {
                    //hanging for they
                    weCount += gameType/2;
                    if(restTheyFlag) {
                        restTheyCount += gameType/2;
                    } else {
                        restTheyCount = gameType/2;
                        restTheyFlag = true;
                    }

                    weCurrentCount = gameType/2;
                    theyCurrentCount = 0;
                    String result = "hanging";
                    writeGameToList(result);
                }
                else {
                    //good game for they
                    y = gameType - x;
                    weCount += x;
                    theyCount += y;
                    if(restWeFlag) {
                        theyCount += restWeCount;
                        restAddition = restWeCount;
                        restWeFlag = false;
                    }
                    if(restTheyFlag) {
                        theyCount += restTheyCount;
                        restAddition = restTheyCount;
                        restTheyFlag = false;
                    }

                    weCurrentCount = x;
                    theyCurrentCount = y + restAddition;
                    String result = "normalGame";
                    writeGameToList(result);
                }
                counterGame++;
                xFlag = true;
                break;
            case THEYPLAY_THEYCOUNT:
                if(x > gameType) {
                    if(x == 1001) {
                        Log.d(TAG, "End of game");
                    } else {
                        Log.d(TAG, "Wrong input");
                    }
                }
                else if(x == gameType) {
                    //clear game for they
                    theyCount += gameType;
                    weCount -= fine;
                    if(restWeFlag) {
                        theyCount += restWeCount;
                        restAddition = restWeCount;
                        restWeFlag = false;
                    }
                    if(restTheyFlag) {
                        theyCount += restTheyCount;
                        restAddition = restTheyCount;
                        restTheyFlag = false;
                    }

                    weCurrentCount = 0;
                    theyCurrentCount = gameType + restAddition;
                    String result = "clearGame";
                    writeGameToList(result);
                }
                else if(x > gameType/2) {
                    //good game for they
                    y = gameType - x;
                    theyCount += x;
                    weCount += y;
                    if(restWeFlag) {
                        theyCount += restWeCount;
                        restAddition = restWeCount;
                        restWeFlag = false;
                    }
                    if(restTheyFlag) {
                        theyCount += restTheyCount;
                        restAddition = restTheyCount;
                        restTheyFlag = false;
                    }

                    weCurrentCount = y;
                    theyCurrentCount = x + restAddition;
                    String result = "normalGame";
                    writeGameToList(result);
                }
                else if(x == gameType/2) {
                    //hanging for they
                    weCount += gameType/2;
                    if(restTheyFlag) {
                        restTheyCount += gameType/2;
                    } else {
                        restTheyCount = gameType/2;
                        restTheyFlag = true;
                    }

                    weCurrentCount = gameType/2;
                    theyCurrentCount = 0;
                    String result = "hanging";
                    writeGameToList(result);
                }
                else if(x == 0) {
                    //full fail for they bait and clear game for we
                    theyCount -= fine;
                    weCount += gameType;
                    baitTheyCount++;
                    if(baitTheyCount > 2) {
                        baitTheyCount = 0;
                        theyCount -= fine;
                    }
                    if(restWeFlag) {
                        weCount += restWeCount;
                        restAddition = restWeCount;
                        restWeFlag = false;
                    }
                    if(restTheyFlag) {
                        weCount += restTheyCount;
                        restAddition = restTheyCount;
                        restTheyFlag = false;
                    }

                    weCurrentCount = gameType + restAddition;
                    theyCurrentCount = 0;
                    String result = "failClearGame";
                    writeGameToList(result);
                }
                else {
                    //bait for they
                    weCount += gameType;
                    baitTheyCount++;
                    if(baitTheyCount > 2) {
                        baitTheyCount = 0;
                        theyCount -= fine;
                    }
                    if(restWeFlag) {
                        weCount += restWeCount;
                        restAddition = restWeCount;
                        restWeFlag = false;
                    }
                    if(restTheyFlag) {
                        weCount += restTheyCount;
                        restAddition = restTheyCount;
                        restTheyFlag = false;
                    }

                    weCurrentCount = gameType + restAddition;
                    theyCurrentCount = 0;
                    String result = "bait";
                    writeGameToList(result);
                }
                counterGame++;
                xFlag = true;
                break;
            case UNDEFINED:
                Log.d(TAG,"Undefined decision");
            default :
                Log.d(TAG, "Without variants");
        }
        weCountTv.setText(Integer.toString(weCount));
        theyCountTv.setText(Integer.toString(theyCount));
        countTv.setText("0");

        if(weCount >= 1001 || theyCount >= 1001) {
            if(weCount > theyCount) {
                winName = weName;
                Log.d(TAG,"WeCount win!");
            }
            else if(theyCount > weCount) {
                winName = theyName;
                Log.d(TAG,"theyCount win!");
            }
            else {
                Log.d(TAG,"draw");
            }
            Intent mIntent = new Intent(this, EndGameActivity.class);
            //mIntent.putParcelableArrayListExtra("data", (ArrayList<? extends android.os.Parcelable>) listGames);
            //mIntent.putParcelableArrayListExtra("data",
            //(ArrayList<? extends android.os.Parcelable>) listGames);
            mIntent.putExtra("NameWe", weName);
            mIntent.putExtra("NameThey", theyName);
            mIntent.putExtra("NameWin", winName);
            mIntent.putExtra("ScoreWe", Integer.toString(weCount));
            mIntent.putExtra("ScoreThey", Integer.toString(theyCount));
            startActivity(mIntent);
        }
    }

    void resetAll() {
        x = 0;
        y = 0;
        counterGame = 0;
        weCount = 0;
        theyCount = 0;
        restWeCount = 0;
        restTheyCount = 0;
        restWeFlag = false;
        restTheyFlag = false;
        xFlag = false;
        weCountTv.setText("0");
        theyCountTv.setText("0");
        countTv.setText("0");
        gameDistrTv.setText("1");
        listGames.clear();
    }

    void loadNames() {
        sPref = getSharedPreferences(NAMES, MODE_PRIVATE);
        weName = sPref.getString(SAVED_NAME_1, "");
        theyName = sPref.getString(SAVED_NAME_2, "");
        weBtn.setText(weName);
        theyBtn.setText(theyName);
        Toast.makeText(this, "Names loaded", Toast.LENGTH_SHORT).show();
    }

    void writeGameToList(String result) {
        Map<String, String> map = new HashMap<String, String>();
        map.put("Result", result);
        map.put("We",Integer.toString(weCurrentCount));
        map.put("They",Integer.toString(theyCurrentCount));
        map.put("Game", Integer.toString(counterGame));
        map.put("Game Type", Integer.toString(gameType));
        listGames.add(map);
        System.out.println(listGames);
        Log.d(TAG, " writeGameToList add game to listGames");
    }

    void removeLastGame() {
        if(counterGame > 0) {
            Map<String, String> map = new HashMap<String, String>();
            map = listGames.get(listGames.size() - 1);
            String str = map.get("Result");
            String tempWe = map.get("We");
            String tempThey = map.get("They");

            if(str.equals("bait")) {
                if(tempWe.equals("0")) {
                    baitWeCount--;
                }
                if(tempThey.equals("0")) {
                    baitTheyCount--;
                }
            }
            if(str.equals("hanging")) {
                if(tempWe.equals("0")) {
                    if(restAddition != 0) {
                        restAddition = 0;
                    }

                    restWeFlag = false;
                }

                if(tempThey.equals("0")) {
                    if(restAddition != 0) {
                        restAddition = 0;
                    }

                    restTheyFlag = false;
                }
            }
            if(str.equals("failClearGame")) {
                if(tempWe.equals("0")){
                    weCount += fine;
                }
                if(tempThey.equals("0")) {
                    theyCount += fine;
                }
            }

            weCount -= weCurrentCount;
            theyCount -= theyCurrentCount;
            counterGame--;
            listGames.remove(listGames.size() - 1);
            weBtn.setText(Integer.toString(weCount));
            theyBtn.setText(Integer.toString(theyCount));
            if(counterGame == 0) {
                gameDistrTv.setText(Integer.toString(counterGame + 1));
            } else {
                gameDistrTv.setText(Integer.toString(counterGame));
            }
            Log.d(TAG, " removeLastGame remove last game from listGames");
        } else {
            Log.d(TAG, " removeLastGame no games");
        }

    }

    void clearDigit() {
        if(x != 0) {
            String backSpace = (String) countTv.getText();
            int lengOfString = backSpace.length();
            if(lengOfString == 1) {
                countTv.setText("0");
                x = 0;
                return;
            } else {
                backSpace = backSpace.substring(0 ,lengOfString - 1);
                x = Integer.parseInt(backSpace);
                countTv.setText(backSpace);
            }
        }
    }

    void baitWarning() {
        AlertDialog.Builder builder = new AlertDialog.Builder(GameActivity.this);
        builder.setTitle("DC Warning!")
                .setMessage("bait")
                //.setIcon(R.drawable.ic_android)
                .setCancelable(false)
                .setNegativeButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    void clearGameWarning() {
        AlertDialog.Builder builder = new AlertDialog.Builder(GameActivity.this);
        builder.setTitle("DC Warning!")
                .setMessage("clearGame -100 from opponents")
                        //.setIcon(R.drawable.ic_android)
                .setCancelable(false)
                .setNegativeButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
        AlertDialog alert = builder.create();
        alert.show();
    }
}