package com.example.dcapp.dcapp;

import android.app.Activity;
import android.os.Bundle;
import android.os.Parcelable;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class EndGameActivity extends Activity {
    String weName, theyName, winName, weScore, theyScore;
    TextView winTv, weScoreTitleTv, theyScoreTitleTv, weScoreTv, theyScoreTv;
    Button revengeBtn, mainMenuBtn;
    List<Map<String, String>> historyOfGame;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_endgame);

        historyOfGame = new ArrayList<Map<String, String>>();

        weName = this.getIntent().getExtras().getString("NameWe");
        theyName = this.getIntent().getExtras().getString("NameThey");
        winName = this.getIntent().getExtras().getString("NameWin");
        weScore = this.getIntent().getExtras().getString("ScoreWe");
        theyScore = this.getIntent().getExtras().getString("ScoreThey");

        winTv = (TextView) findViewById(R.id.winTv);
        weScoreTitleTv = (TextView) findViewById(R.id.weScoreTitleTv);
        theyScoreTitleTv = (TextView) findViewById(R.id.theyScoreTitleTv);
        weScoreTv = (TextView) findViewById(R.id.weScoreTv);
        theyScoreTv = (TextView) findViewById(R.id.theyScoreTv);
        revengeBtn = (Button) findViewById(R.id.revengeBtn);
        mainMenuBtn = (Button) findViewById(R.id.mainMenuBtn);

        //Bundle bdl = getIntent().getExtras();
        //historyOfGame = bdl.getParcelableArrayList("data");

        winTv.setText(winName);
        weScoreTitleTv.setText(weName);
        theyScoreTitleTv.setText(theyName);
        weScoreTv.setText(weScore);
        theyScoreTv.setText(theyScore);
    }
}


