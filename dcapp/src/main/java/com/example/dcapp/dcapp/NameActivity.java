package com.example.dcapp.dcapp;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class NameActivity extends Activity {
    TextView annotationFirst;
    TextView annotationSecond;
    EditText pairFirst;
    EditText pairSecond;
    Button toStartGame;
    String pairNameFirst;
    String pairNameSecond;
    private static final String NAMES = "names";
    SharedPreferences sPref;

    final String SAVED_NAME_1 = "saved_name_we";
    final String SAVED_NAME_2 = "saved_name_they";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_name);

        pairFirst = (EditText) findViewById(R.id.pairFirst);
        pairSecond = (EditText) findViewById(R.id.pairSecond);

        toStartGame = (Button) findViewById(R.id.start_game);

        toStartGame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String checkPairFirst = pairFirst.getText().toString();
                String checkPairSecond = pairSecond.getText().toString();

                if(checkPairFirst.matches("")) {
                    pairNameFirst = "We";
                } else {
                    pairNameFirst = pairFirst.getText().toString();
                }

                if(checkPairSecond.matches("")) {
                    pairNameSecond = "They";
                } else {
                    pairNameSecond = pairSecond.getText().toString();
                }

                saveNames();

                Log.d("DC NameActivity", " startGameButton Clicked");

                Intent intent = new Intent(view.getContext(), GameActivity.class);
                startActivity(intent);
            }
        });

        loadNames();
    }

    void saveNames() {
        sPref = getSharedPreferences(NAMES, MODE_PRIVATE);
        Editor ed = sPref.edit();
        ed.putString(SAVED_NAME_1, pairNameFirst);
        ed.putString(SAVED_NAME_2, pairNameSecond);
        ed.commit();
        Toast.makeText(this, "Names saved", Toast.LENGTH_SHORT).show();
    }

    void loadNames() {
        sPref = getSharedPreferences(NAMES, MODE_PRIVATE);
        String savedNameFirst = sPref.getString(SAVED_NAME_1, "");
        String savedNameSecond = sPref.getString(SAVED_NAME_2, "");
        pairFirst.setText(savedNameFirst);
        pairSecond.setText(savedNameSecond);
        Toast.makeText(this, "Names loaded", Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onDestroy() {
        saveNames();
        super.onDestroy();
    }
}